package com.ciricuervo.detectingpatterns;

/**
 * @author ciricuervo
 *
 */
public class Array {

	private static final int MAX_NEW_ROWS = 200;

	private int rows;
	private int cols;
	private int[][] array;

	public Array(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		this.array = new int[rows + MAX_NEW_ROWS][cols];
	}

	public int getRows()
	{
		return rows;
	}

	public void setRows(int rows)
	{
		this.rows = rows;
	}

	public int getCols()
	{
		return cols;
	}

	public void setCols(int cols)
	{
		this.cols = cols;
	}

	public int getValue(int rowIndex, int colIndex)
	{
		return array[rowIndex][colIndex];
	}

	public void setValue(int rowIndex, int colIndex, int value)
	{
		array[rowIndex][colIndex] = value;
	}

}
