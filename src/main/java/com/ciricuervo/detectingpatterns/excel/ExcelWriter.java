package com.ciricuervo.detectingpatterns.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ciricuervo.detectingpatterns.Array;
import com.ciricuervo.detectingpatterns.AutorunCollection;
import com.ciricuervo.detectingpatterns.AutorunCollection.Match;
import com.ciricuervo.detectingpatterns.Pattern;
import com.ciricuervo.detectingpatterns.PatternsCollection;
import com.ciricuervo.detectingpatterns.PatternsDetector;
import com.ciricuervo.detectingpatterns.msg.Messages;

/**
 * @author ciricuervo
 *
 */
public class ExcelWriter {

	public static final int MULTIVALUED_CELL = 2;

	public static String saveArray(Array array, PatternsCollection patterns, String path)
			throws IOException
	{
		FileOutputStream fileOut = null;

		try
		{
			XSSFWorkbook wb = new XSSFWorkbook();

			// create first sheet
			CreationHelper createHelper = wb.getCreationHelper();
			Sheet sheet = wb.createSheet(Messages.getString("Excel.sheet", 1));

			// print the original values
			for (int rowIndex = 0; rowIndex < array.getRows(); rowIndex++)
			{
				Row row = sheet.createRow(rowIndex);
				for (int colIndex = 0; colIndex < array.getCols(); colIndex++)
				{
					Cell cell = row.createCell(colIndex);

					int value = array.getValue(rowIndex, colIndex);
					cell.setCellValue(value);

					if (PatternsDetector.isInArrayDomain(value))
					{
						cell.setCellStyle(getDefaultStyle(wb));
					} else
					{
						cell.setCellStyle(getInvalidValueStyle(wb));
					}
				}
			}

			// print solution
			Row row = sheet.createRow(array.getRows());
			for (int colIndex = 0; colIndex < array.getCols(); colIndex++)
			{
				LinkedList<Pattern> result = patterns.get(colIndex);
				Set<Integer> values = patterns.getValues(colIndex);
				long more = patterns.getPatternsCount(colIndex) - result.size();

				Cell cell = row.createCell(colIndex);
				cell.setCellStyle(getSolutionStyle(wb));

				if (values.size() == 1)
				{
					cell.setCellValue(values.iterator().next());
				} else
				{
					printMultivaluedCell(createHelper, cell, values);
				}

				int colIndexDetail = colIndex + array.getCols() + 1;

				// print all recognized patterns for this solution
				int rowIndex = array.getRows();

				for (Pattern pattern : result)
				{
					Row newRow = sheet.getRow(rowIndex);
					if (newRow == null)
					{
						newRow = sheet.createRow(rowIndex);
					}
					pattern.invert();
					newRow.createCell(colIndexDetail).setCellValue(
							createHelper.createRichTextString(pattern.toString()));
					rowIndex++;
				}

				if (more > 0)
				{
					Row newRow = sheet.getRow(rowIndex);
					if (newRow == null)
					{
						newRow = sheet.createRow(rowIndex);
					}

					newRow.createCell(colIndexDetail).setCellValue(
							createHelper.createRichTextString(Messages.getString("Other.andMore",
									more)));
				}
			}

			// set columns widths
			for (int colIndex = 0; colIndex < array.getCols(); colIndex++)
			{
				sheet.setColumnWidth(colIndex, 1000);
				sheet.setColumnWidth(colIndex + array.getCols() + 1, 5000);
			}

			// save result
			path = renameFile(path);
			fileOut = new FileOutputStream(path);
			wb.write(fileOut);
			return path;
		} finally
		{
			if (fileOut != null)
			{
				fileOut.close();
			}
		}
	}

	public static String saveArray(Array array, AutorunCollection collection, String path)
			throws IOException
	{
		FileOutputStream fileOut = null;

		try
		{
			XSSFWorkbook wb = new XSSFWorkbook();

			// create first sheet
			CreationHelper createHelper = wb.getCreationHelper();
			Sheet sheet = wb.createSheet(Messages.getString("Excel.sheet", 1));

			// print the original values
			for (int rowIndex = 0; rowIndex < array.getRows(); rowIndex++)
			{
				Row row = sheet.createRow(rowIndex);
				for (int colIndex = 0; colIndex < array.getCols(); colIndex++)
				{
					int value = array.getValue(rowIndex, colIndex);
					Cell cell = row.createCell(colIndex);
					cell.setCellValue(value);
					cell.setCellStyle(getDefaultStyle(wb));

					if (rowIndex < PatternsDetector.MIN_ROWS)
					{
						continue;
					}

					// print solution
					int collectionRowIndex = rowIndex - PatternsDetector.MIN_ROWS;
					Pattern pattern = collection.getPattern(collectionRowIndex, colIndex);

					int colIndexSolution = colIndex + array.getCols() + 1;
					Cell cellSolution = row.createCell(colIndexSolution);

					Match patternMatch = collection.getPatternMatch(collectionRowIndex, colIndex);
					Match rowMatch = collection.getRowMatch(collectionRowIndex);

					if (Match.MULTI.equals(patternMatch))
					{
						printMultivaluedCell(createHelper, cellSolution, null);
					} else if (pattern != null)
					{
						cellSolution.setCellValue(pattern.getValue());
					}

					if (Match.FALSE.equals(rowMatch))
					{
						cellSolution.setCellStyle(getDefaultStyle(wb));
					} else
					{
						cellSolution.setCellStyle(getValidValueStyle(wb));
					}

					if (pattern == null)
					{
						continue;
					}

					int colIndexLength = colIndex + 2 * (array.getCols() + 1);
					Cell cellLength = row.createCell(colIndexLength);
					cellLength.setCellValue(pattern.getDistance());
					cellLength.setCellStyle(getDefaultStyle(wb));

					if (pattern.getLength() == 0)
					{
						continue;
					}

					pattern.invert();

					int colIndexDetail = colIndex + 3 * (array.getCols() + 1);
					Cell cellDetail = row.createCell(colIndexDetail);
					cellDetail.setCellValue(createHelper.createRichTextString(pattern.toString()));
					cellDetail.setCellStyle(getDefaultStyle(wb));
				}
			}

			// set columns widths
			for (int colIndex = 0; colIndex < array.getCols(); colIndex++)
			{
				sheet.setColumnWidth(colIndex, 1000);
				sheet.setColumnWidth(colIndex + array.getCols() + 1, 1000);
				sheet.setColumnWidth(colIndex + 2 * (array.getCols() + 1), 1000);
				sheet.setColumnWidth(colIndex + 3 * (array.getCols() + 1), 5000);
			}

			// save result
			path = renameFile(path);
			fileOut = new FileOutputStream(path);
			wb.write(fileOut);
			return path;
		} finally
		{
			if (fileOut != null)
			{
				fileOut.close();
			}
		}
	}

	private static XSSFCellStyle getDefaultStyle(XSSFWorkbook wb)
	{
		XSSFCellStyle cs = wb.createCellStyle();
		cs.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		return cs;
	}

	private static CellStyle getInvalidValueStyle(XSSFWorkbook wb)
	{
		XSSFFont font = wb.createFont();
		font.setColor(IndexedColors.RED.getIndex());
		XSSFCellStyle cs = wb.createCellStyle();
		cs.setFont(font);
		cs.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		return cs;
	}

	private static CellStyle getValidValueStyle(XSSFWorkbook wb)
	{
		XSSFCellStyle cs = wb.createCellStyle();
		cs.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		cs.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		cs.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		return cs;
	}

	private static XSSFCellStyle getSolutionStyle(XSSFWorkbook wb)
	{
		XSSFFont font = wb.createFont();
		font.setBold(true);
		XSSFCellStyle cs = wb.createCellStyle();
		cs.setFont(font);
		cs.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		cs.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		cs.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		return cs;
	}

	@SuppressWarnings("unused")
	private static void printMultivaluedCell(CreationHelper createHelper, Cell cell,
			Set<Integer> values)
	{
		if (MULTIVALUED_CELL != -1)
		{
			cell.setCellValue(MULTIVALUED_CELL);
		} else if (values != null)
		{
			StringBuilder valueText = new StringBuilder(16);
			for (Integer integer : values)
			{
				if (valueText.length() != 0)
				{
					valueText.append("/");
				}
				valueText.append(integer.toString());
			}
			cell.setCellValue(createHelper.createRichTextString(valueText.toString()));
		}
	}

	private static String renameFile(String path)
	{
		int dot = path.lastIndexOf(".");
		String fileName = path.substring(0, dot);
		String extension = path.substring(dot + 1);

		int countFiles = 2;
		while (new File(path).exists())
		{
			String sufix = countFiles > 0 ? " (" + countFiles + ")" : "";
			path = fileName + sufix + "." + extension;
			countFiles++;
		}

		return path;
	}

}
