package com.ciricuervo.detectingpatterns.excel;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ciricuervo.detectingpatterns.Array;
import com.ciricuervo.detectingpatterns.PatternsDetector;

/**
 * @author ciricuervo
 *
 */
public class ExcelReader {

	public static Array loadArray(File sourceFile) throws InvalidFormatException, IOException
	{
		OPCPackage pkg = null;

		try
		{
			// open excel resource
			pkg = OPCPackage.open(sourceFile, PackageAccess.READ);
			@SuppressWarnings("resource")
			XSSFWorkbook wb = new XSSFWorkbook(pkg);

			// get first sheet
			Sheet sheet = wb.getSheetAt(0);

			// get the width and height of the array
			int rows = sheet.getPhysicalNumberOfRows();
			int cols = 0;

			if (rows > 0)
			{
				cols = sheet.getRow(sheet.getFirstRowNum()).getPhysicalNumberOfCells();
				cols = Math.min(cols, PatternsDetector.MAX_COLS);
			}

			Array array = new Array(rows, cols);

			int lastWrittenRow = -1;

			// read all cells
			for (Row row : sheet)
			{
				for (Cell cell : row)
				{
					int rowIndex = cell.getRowIndex();
					int colIndex = cell.getColumnIndex();

					if (colIndex >= cols)
					{
						break;
					}

					try
					{
						int value = (int) cell.getNumericCellValue();

						if (value < 0 || value > 9)
						{
							throw new RuntimeException("The value must be between 0 and 9");
						}

						array.setValue(rowIndex, colIndex, value);
					} catch (Exception e)
					{
						System.err.println("Error reading cell " + rowIndex + ", " + colIndex);
						e.printStackTrace();

						// set the amount of rows
						array.setRows(rowIndex);
						return array;
					}

					lastWrittenRow = rowIndex;
				}
			}

			// set the amount of rows
			array.setRows(lastWrittenRow + 1);
			return array;
		} finally
		{
			if (pkg != null)
			{
				pkg.revert();
			}
		}
	}

}
