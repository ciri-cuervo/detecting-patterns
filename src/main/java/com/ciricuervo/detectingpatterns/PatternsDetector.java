package com.ciricuervo.detectingpatterns;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.eclipse.swt.widgets.Display;

import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.tasks.PatternsAutorunTask;
import com.ciricuervo.detectingpatterns.tasks.PatternsDetectorTask;
import com.ciricuervo.detectingpatterns.tasks.PatternsDisplayTask;
import com.ciricuervo.detectingpatterns.ui.display.DisplayPattern;
import com.ciricuervo.detectingpatterns.ui.display.DisplayStatus;

/**
 * @author ciricuervo
 *
 */
public class PatternsDetector {

	public static final int[] ARRAY_DOMAIN = { 0, 1 };
	public static final int MIN_ROWS = 15;
	public static final int MAX_COLS = 10;
	public static final int MAX_PATTERNS_QUEUE = 10000;

	private static final ThreadPoolExecutor THREAD_POOL = new ThreadPoolExecutor(MAX_COLS
			* ARRAY_DOMAIN.length, Integer.MAX_VALUE, 10L, TimeUnit.SECONDS,
			new SynchronousQueue<Runnable>());

	static
	{
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run()
			{
				try
				{
					THREAD_POOL.shutdownNow();
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public static PatternsCollection detectPatterns(Array array)
	{
		PatternsCollection collection = new PatternsCollection(array.getCols());

		for (int length = 2; length <= array.getCols(); length++)
		{
			String status = Messages.getString("Other.row", array.getRows() + 1) + " - "
					+ Messages.getString("Status.analyzingPatternsLength", length);
			Display.getDefault().asyncExec(new DisplayStatus(status));

			// first get all patterns
			getPatters(array, length, array.getRows(), collection);

			// then process result and display
			List<PatternsDisplayTask> displayTasks = new ArrayList<PatternsDisplayTask>(
					array.getCols());
			for (int colIndex = 0; colIndex < array.getCols(); colIndex++)
			{
				displayTasks.add(new PatternsDisplayTask(colIndex, collection));
			}
			List<DisplayPattern> displayTasksResult = executeTasks(displayTasks);

			for (DisplayPattern displayPattern : displayTasksResult)
			{
				Display.getDefault().asyncExec(displayPattern);
			}
		}

		return collection;
	}

	public static AutorunCollection autorunPatterns(Array array)
	{
		AutorunCollection collection = new AutorunCollection(
				Math.max(array.getRows() - MIN_ROWS, 0), array.getCols());

		for (int rowIndex = MIN_ROWS; rowIndex < array.getRows(); rowIndex++)
		{
			String status = Messages.getString("Other.row", rowIndex + 1) + " - "
					+ Messages.getString("Status.analyzingPatterns");
			Display.getDefault().asyncExec(new DisplayStatus(status));

			PatternsCollection patterns = new PatternsCollection(array.getCols());

			for (int length = 2; length <= array.getCols(); length++)
			{
				// first get all patterns
				getPatters(array, length, rowIndex, patterns);
			}

			// then process result
			int collectionRowIndex = rowIndex - MIN_ROWS;
			List<PatternsAutorunTask> autorunTasks = new ArrayList<PatternsAutorunTask>(
					array.getCols());
			for (int colIndex = 0; colIndex < array.getCols(); colIndex++)
			{
				autorunTasks.add(new PatternsAutorunTask(collectionRowIndex, colIndex, array
						.getValue(rowIndex, colIndex), collection, patterns));
			}
			executeTasks(autorunTasks);
		}

		return collection;
	}

	private static void getPatters(Array array, int length, int rowIndex,
			PatternsCollection collection)
	{
		List<Callable<Pattern>> tasks = new ArrayList<Callable<Pattern>>(array.getCols()
				* ARRAY_DOMAIN.length);
		for (int colIndex = 0; colIndex < array.getCols(); colIndex++)
		{
			for (int value : ARRAY_DOMAIN)
			{
				tasks.add(new PatternsDetectorTask(array, length, value, colIndex, rowIndex,
						collection));
			}
		}
		executeTasks(tasks);
	}

	private static <E> List<E> executeTasks(List<? extends Callable<E>> tasks)
	{
		List<E> elements = new ArrayList<E>(tasks.size());

		try
		{
			List<Future<E>> futures = THREAD_POOL.invokeAll(tasks, 120, TimeUnit.MINUTES);

			for (Future<E> future : futures)
			{
				if (future.isDone() && !future.isCancelled())
				{
					try
					{
						elements.add(future.get());
					} catch (ExecutionException e)
					{
						e.printStackTrace();
					}
				}
			}
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		return elements;
	}

	public static boolean isInArrayDomain(int value)
	{
		for (int elem : ARRAY_DOMAIN)
		{
			if (value == elem)
			{
				return true;
			}
		}
		return false;
	}

}
