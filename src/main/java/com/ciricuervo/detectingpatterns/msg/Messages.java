package com.ciricuervo.detectingpatterns.msg;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import com.ciricuervo.detectingpatterns.ui.Application;

/**
 * @author ciricuervo
 *
 */
public class Messages {

	private static final String BUNDLE_NAME = Application.RESOURCES_DIR + "messages"; //$NON-NLS-1$
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	public static String getString(String key, Object... params)
	{
		try
		{
			String str = RESOURCE_BUNDLE.getString(key);
			for (int i = 0; i < params.length; i++)
			{
				str = str.replace("{" + i + "}", params[i].toString()); //$NON-NLS-1$ //$NON-NLS-2$
			}
			return str;
		} catch (MissingResourceException e)
		{
			return '!' + key + '!';
		}
	}

}
