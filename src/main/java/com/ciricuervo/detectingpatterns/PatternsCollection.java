package com.ciricuervo.detectingpatterns;

import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author ciricuervo
 *
 */
public class PatternsCollection {

	public static final Comparator<? super Pattern> PATTERNS_CMP = new Comparator<Pattern>() {

		@Override
		public int compare(Pattern p1, Pattern p2)
		{
			// length of pattern, descendant order
			if (p1.getLength() > p2.getLength())
			{
				return -1;
			}
			if (p1.getLength() < p2.getLength())
			{
				return 1;
			}

			// value, ascendant order
			if (p1.getValue() < p2.getValue())
			{
				return -1;
			}
			if (p1.getValue() > p2.getValue())
			{
				return 1;
			}

			return 0;
		}
	};

	private LinkedList<Pattern>[] patternsList;
	private boolean[] patternsListChanged;
	private Set<Integer>[] uniqueValues;
	private long[] patternsCount;

	@SuppressWarnings("unchecked")
	public PatternsCollection(int cols) {
		this.patternsList = new LinkedList[cols];
		this.uniqueValues = new Set[cols];
		this.patternsListChanged = new boolean[cols];
		this.patternsCount = new long[cols];

		for (int i = 0; i < cols; i++)
		{
			this.patternsList[i] = new LinkedList<Pattern>();
			this.uniqueValues[i] = new TreeSet<Integer>();
			this.patternsListChanged[i] = false;
			this.patternsCount[i] = 0;
		}
	}

	// this method is used by several threads per colIndex
	public void add(int colIndex, Pattern pattern)
	{
		Deque<Pattern> patterns = patternsList[colIndex];

		synchronized (patterns)
		{
			if (!patterns.isEmpty())
			{
				if (pattern.getDistance() < patterns.peekFirst().getDistance())
				{
					return;
				}
				if (pattern.getDistance() > patterns.peekFirst().getDistance())
				{
					patterns.clear();
					uniqueValues[colIndex].clear();
					patternsCount[colIndex] = 0;
				}
			}

			if (patterns.size() == PatternsDetector.MAX_PATTERNS_QUEUE)
			{
				patterns.pollFirst();
			}

			patterns.addLast(pattern);
			uniqueValues[colIndex].add(pattern.getValue());
			patternsListChanged[colIndex] = true;
			patternsCount[colIndex] = patternsCount[colIndex] + 1;
		}
	}

	// this method is used only by a thread per colIndex
	public LinkedList<Pattern> get(int colIndex)
	{
		if (patternsListChanged[colIndex])
		{
			Collections.sort(patternsList[colIndex], PATTERNS_CMP);
			patternsListChanged[colIndex] = false;
		}
		return patternsList[colIndex];
	}

	public Set<Integer> getValues(int colIndex)
	{
		if (!uniqueValues[colIndex].isEmpty())
		{
			return uniqueValues[colIndex];
		}

		Set<Integer> values = new TreeSet<Integer>();
		for (int value : PatternsDetector.ARRAY_DOMAIN)
		{
			values.add(value);
		}
		return values;
	}

	public long getPatternsCount(int colIndex)
	{
		return patternsCount[colIndex];
	}

}
