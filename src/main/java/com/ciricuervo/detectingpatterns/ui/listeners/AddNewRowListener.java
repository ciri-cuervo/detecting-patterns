package com.ciricuervo.detectingpatterns.ui.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;

import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.ui.Application;
import com.ciricuervo.detectingpatterns.ui.UiElements;
import com.ciricuervo.detectingpatterns.ui.display.DisplayStatus;

/**
 * @author ciricuervo
 *
 */
public class AddNewRowListener extends SelectionAdapter {

	@Override
	public void widgetSelected(SelectionEvent e)
	{
		if (UiElements.txtsNewRow.isEmpty())
		{
			return;
		}

		// first analyze all values
		for (Text text : UiElements.txtsNewRow)
		{
			try
			{
				Integer.parseInt(text.getText());

			} catch (Exception e1)
			{
				String status = Messages.getString("Status.invalidInput", text.getText());
				Display.getDefault().asyncExec(new DisplayStatus(status));
				text.setFocus();
				return;
			}
		}

		// then copy the values
		int rowIndex = UiElements.array.getRows();
		for (int colIndex = 0; colIndex < UiElements.txtsNewRow.size(); colIndex++)
		{
			Text text = UiElements.txtsNewRow.get(colIndex);
			int value = Integer.parseInt(text.getText());
			UiElements.array.setValue(rowIndex, colIndex, value);
			text.setText("");
		}
		UiElements.array.setRows(rowIndex + 1);

		Application.cleanCandidateRowItems();
		Application.refreshLastRows();
		UiElements.txtsNewRow.get(0).setFocus();

		String status = Messages.getString("Status.rowAdded");
		Display.getDefault().asyncExec(new DisplayStatus(status));
	}

}
