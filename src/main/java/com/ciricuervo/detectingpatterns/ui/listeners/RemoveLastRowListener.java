package com.ciricuervo.detectingpatterns.ui.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;

import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.ui.Application;
import com.ciricuervo.detectingpatterns.ui.UiElements;
import com.ciricuervo.detectingpatterns.ui.display.DisplayStatus;

/**
 * @author ciricuervo
 *
 */
public class RemoveLastRowListener extends SelectionAdapter {

	@Override
	public void widgetSelected(SelectionEvent e)
	{
		if (UiElements.array == null || UiElements.array.getRows() == 0)
		{
			return;
		}

		StringBuilder text = new StringBuilder(16);
		int rowIndex = UiElements.array.getRows() - 1;
		for (int colIndex = 0; colIndex < UiElements.txtsNewRow.size(); colIndex++)
		{
			String value = String.valueOf(UiElements.array.getValue(rowIndex, colIndex));
			text.append(value);
		}
		UiElements.array.setRows(rowIndex);

		Application.cleanCandidateRowItems();
		Application.refreshLastRows();
		UiElements.txtsNewRow.get(0).setFocus();

		String status = Messages.getString("Status.rowRemoved", text.toString());
		Display.getDefault().asyncExec(new DisplayStatus(status));
	}

}
