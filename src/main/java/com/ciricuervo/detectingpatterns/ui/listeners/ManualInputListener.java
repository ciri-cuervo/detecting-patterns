package com.ciricuervo.detectingpatterns.ui.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;

import com.ciricuervo.detectingpatterns.Array;
import com.ciricuervo.detectingpatterns.PatternsDetector;
import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.ui.Application;
import com.ciricuervo.detectingpatterns.ui.ManualInputDialog;
import com.ciricuervo.detectingpatterns.ui.UiElements;
import com.ciricuervo.detectingpatterns.ui.display.DisplayStatus;

/**
 * @author ciricuervo
 *
 */
public class ManualInputListener extends SelectionAdapter {

	@Override
	public void widgetSelected(SelectionEvent e)
	{
		ManualInputDialog dialog = new ManualInputDialog(UiElements.shlDetectingPatterns);
		int cols = dialog.open();

		if (cols < 2 || cols > PatternsDetector.MAX_COLS)
		{
			String status = Messages.getString("Status.maxCols", PatternsDetector.MAX_COLS);
			Display.getDefault().asyncExec(new DisplayStatus(status));
			return;
		}

		try
		{
			UiElements.array = new Array(0, cols);
			Application.createComponents();
			Display.getDefault().asyncExec(new DisplayStatus());
		} catch (Exception e1)
		{
			e1.printStackTrace();
		}
	}

}
