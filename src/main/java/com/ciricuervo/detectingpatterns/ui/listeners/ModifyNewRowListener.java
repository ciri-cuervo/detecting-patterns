package com.ciricuervo.detectingpatterns.ui.listeners;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Text;

/**
 * @author ciricuervo
 *
 */
public class ModifyNewRowListener implements ModifyListener {

	private Text nextText;

	public ModifyNewRowListener(Text nextText) {
		this.nextText = nextText;
	}

	@Override
	public void modifyText(ModifyEvent e)
	{
		Text text = (Text) e.widget;
		if (text.getText().length() > 0)
		{
			nextText.setFocus();
		}
	}

}
