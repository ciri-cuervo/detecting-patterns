package com.ciricuervo.detectingpatterns.ui.listeners;

import java.io.File;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.DirectoryDialog;

import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.ui.UiElements;
import com.ciricuervo.detectingpatterns.ui.settings.SettingsManager;

/**
 * @author ciricuervo
 *
 */
public class StorageFolderListener extends SelectionAdapter {

	@Override
	public void widgetSelected(SelectionEvent e)
	{
		DirectoryDialog dialog = new DirectoryDialog(UiElements.shlDetectingPatterns);
		dialog.setFilterPath(SettingsManager.getDownloadsDir());
		dialog.setText(Messages.getString("File.selectDirectory"));
		String path = dialog.open();
		if (path == null || !new File(path).isDirectory())
		{
			return;
		}
		SettingsManager.setDownloadsDir(path);
	}
}
