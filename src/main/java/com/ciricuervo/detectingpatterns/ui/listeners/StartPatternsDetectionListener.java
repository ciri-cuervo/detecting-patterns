package com.ciricuervo.detectingpatterns.ui.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;

import com.ciricuervo.detectingpatterns.PatternsCollection;
import com.ciricuervo.detectingpatterns.PatternsDetector;
import com.ciricuervo.detectingpatterns.excel.ExcelWriter;
import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.ui.Application;
import com.ciricuervo.detectingpatterns.ui.UiElements;
import com.ciricuervo.detectingpatterns.ui.display.DisplayEnable;
import com.ciricuervo.detectingpatterns.ui.display.DisplayFocus;
import com.ciricuervo.detectingpatterns.ui.display.DisplayStatus;
import com.ciricuervo.detectingpatterns.ui.settings.SettingsManager;

/**
 * @author ciricuervo
 *
 */
public class StartPatternsDetectionListener extends SelectionAdapter {

	private Button[] disablingButtons;

	public StartPatternsDetectionListener(Button... disablingButtons) {
		this.disablingButtons = disablingButtons;
	}

	@Override
	public void widgetSelected(SelectionEvent e)
	{
		if (UiElements.array == null || !UiElements.btnStartPatternsDetection.isEnabled())
		{
			return;
		}

		if (UiElements.array.getRows() < PatternsDetector.MIN_ROWS)
		{
			String status = Messages.getString("Status.minRows", PatternsDetector.MIN_ROWS);
			Display.getDefault().asyncExec(new DisplayStatus(status));
			return;
		}

		UiElements.btnStartPatternsDetection.setEnabled(false);
		for (Button button : disablingButtons)
		{
			button.setEnabled(false);
		}

		Application.cleanCandidateRowItems();
		Application.cleanColumnsDetailLabels();

		new Thread(new Runnable() {
			@Override
			public void run()
			{
				try
				{
					long time = System.currentTimeMillis();
					PatternsCollection patterns = PatternsDetector.detectPatterns(UiElements.array);
					long ms = System.currentTimeMillis() - time;

					String status = Messages.getString("Status.analysisFinished", ms) + " - "
							+ Messages.getString("Status.savingReport");
					Display.getDefault().asyncExec(new DisplayStatus(status));

					String report = ExcelWriter.saveArray(UiElements.array, patterns,
							SettingsManager.getReportPath());

					status = Messages.getString("Status.analysisFinished", ms) + " - <A>"
							+ Messages.getString("Status.reportReady") + "</A>";
					Display.getDefault().asyncExec(new DisplayStatus(status, report));

				} catch (Exception e)
				{
					e.printStackTrace();
					String text = Messages.getString("Status.errorInAnalysis");
					Display.getDefault().asyncExec(new DisplayStatus(text));
				} finally
				{
					for (Button button : disablingButtons)
					{
						Display.getDefault().asyncExec(new DisplayEnable(button, true));
					}
					Display.getDefault().asyncExec(
							new DisplayEnable(UiElements.btnStartPatternsDetection, true));

					Display.getDefault().asyncExec(new DisplayFocus(UiElements.btnCopy));
				}
			}
		}).start();
	}
}
