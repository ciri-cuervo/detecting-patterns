package com.ciricuervo.detectingpatterns.ui.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Text;

import com.ciricuervo.detectingpatterns.ui.UiElements;

/**
 * @author ciricuervo
 *
 */
public class CopyToNewRowListener extends SelectionAdapter {

	@Override
	public void widgetSelected(SelectionEvent e)
	{
		Text firstBlank = null;
		for (int i = 2; i < UiElements.tblCandidateRow.getColumnCount(); i++)
		{
			String value = UiElements.tblCandidateRow.getItem(0).getText(i);
			if (value.length() != 1)
			{
				UiElements.txtsNewRow.get(i - 2).setText("");
				if (firstBlank == null)
				{
					firstBlank = UiElements.txtsNewRow.get(i - 2);
				}
			} else
			{
				UiElements.txtsNewRow.get(i - 2).setText(value);
			}
		}

		if (firstBlank != null)
		{
			firstBlank.setFocus();
		}
	}
}
