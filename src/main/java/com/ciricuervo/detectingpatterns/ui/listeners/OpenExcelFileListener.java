package com.ciricuervo.detectingpatterns.ui.listeners;

import java.io.File;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;

import com.ciricuervo.detectingpatterns.excel.ExcelReader;
import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.ui.Application;
import com.ciricuervo.detectingpatterns.ui.UiElements;
import com.ciricuervo.detectingpatterns.ui.display.DisplayStatus;
import com.ciricuervo.detectingpatterns.ui.settings.SettingsManager;

/**
 * @author ciricuervo
 *
 */
public class OpenExcelFileListener extends SelectionAdapter {

	@Override
	public void widgetSelected(SelectionEvent e)
	{
		String status = Messages.getString("Status.loadingFile");
		Display.getDefault().asyncExec(new DisplayStatus(status));

		FileDialog dialog = new FileDialog(UiElements.shlDetectingPatterns);

		dialog.setFilterPath(SettingsManager.getSelectedFileDirectory());
		dialog.setFilterNames(new String[] {
				Messages.getString("File.excelFiles", "(*.xls;*.xlsx)"),
				Messages.getString("File.allFiles") });
		dialog.setFilterExtensions(new String[] { "*.xls*", "*.*" });
		dialog.setText(Messages.getString("File.selectFile"));

		String path = dialog.open();
		if (path == null)
		{
			Display.getDefault().asyncExec(new DisplayStatus());
			return;
		}

		SettingsManager.setSelectedFile(path);

		File arrayFile = new File(path);
		if (arrayFile.isFile())
		{
			try
			{
				UiElements.array = ExcelReader.loadArray(arrayFile);
				Application.createComponents();

				status = Messages.getString("Status.loaded", path);
				Display.getDefault().asyncExec(new DisplayStatus(status));
			} catch (Exception e1)
			{
				e1.printStackTrace();
				status = Messages.getString("Status.errorOpenFile");
				Display.getDefault().asyncExec(new DisplayStatus(status));
			}
		} else
		{
			status = Messages.getString("Status.errorOpenFile");
			Display.getDefault().asyncExec(new DisplayStatus(status));
		}
	}

}
