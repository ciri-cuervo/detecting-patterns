package com.ciricuervo.detectingpatterns.ui.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;

/**
 * @author ciricuervo
 *
 */
public class BackspacePressedListener extends KeyAdapter {

	private Text previousText;

	public BackspacePressedListener(Text previousText) {
		this.previousText = previousText;
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		if (e.keyCode == SWT.BS) // backspace
		{
			if (e.widget instanceof Text)
			{
				Text text = (Text) e.widget;
				if (text.getText().length() == 0)
				{
					previousText.setFocus();
				}
				return;
			}

			if (e.widget instanceof Button)
			{
				previousText.setFocus();
				return;
			}
		}
	}

}
