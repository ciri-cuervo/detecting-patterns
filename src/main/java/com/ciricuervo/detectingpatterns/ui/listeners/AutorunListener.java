package com.ciricuervo.detectingpatterns.ui.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;

import com.ciricuervo.detectingpatterns.AutorunCollection;
import com.ciricuervo.detectingpatterns.PatternsDetector;
import com.ciricuervo.detectingpatterns.excel.ExcelWriter;
import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.ui.Application;
import com.ciricuervo.detectingpatterns.ui.UiElements;
import com.ciricuervo.detectingpatterns.ui.display.DisplayEnable;
import com.ciricuervo.detectingpatterns.ui.display.DisplayStatus;
import com.ciricuervo.detectingpatterns.ui.settings.SettingsManager;

/**
 * @author ciricuervo
 *
 */
public class AutorunListener extends SelectionAdapter {

	private Button[] disablingButtons;

	public AutorunListener(Button... disablingButtons) {
		this.disablingButtons = disablingButtons;
	}

	@Override
	public void widgetSelected(SelectionEvent e)
	{
		if (UiElements.array == null || !UiElements.btnAutorun.isEnabled())
		{
			return;
		}

		if (UiElements.array.getRows() < PatternsDetector.MIN_ROWS)
		{
			String status = Messages.getString("Status.minRows", PatternsDetector.MIN_ROWS);
			Display.getDefault().asyncExec(new DisplayStatus(status));
			return;
		}

		UiElements.btnAutorun.setEnabled(false);
		for (Button button : disablingButtons)
		{
			button.setEnabled(false);
		}

		Application.cleanCandidateRowItems();
		Application.cleanColumnsDetailLabels();

		new Thread(new Runnable() {
			@Override
			public void run()
			{
				try
				{
					long time = System.currentTimeMillis();
					AutorunCollection collection = PatternsDetector
							.autorunPatterns(UiElements.array);
					long ms = System.currentTimeMillis() - time;

					String status = Messages.getString("Status.analysisFinished", ms) + " - "
							+ Messages.getString("Status.savingReport");
					Display.getDefault().asyncExec(new DisplayStatus(status));

					String report = ExcelWriter.saveArray(UiElements.array, collection,
							SettingsManager.getAutorunReportPath());

					status = Messages.getString("Status.analysisFinished", ms)
							+ " - <A>"
							+ Messages.getString("Status.reportReady")
							+ "</A> - "
							+ Messages.getString(
									collection.getMatchingRows() == 1 ? "Status.matchingRow"
											: "Status.matchingRows", collection.getMatchingRows());
					Display.getDefault().asyncExec(new DisplayStatus(status, report));

				} catch (Exception e)
				{
					e.printStackTrace();
					String status = Messages.getString("Status.errorInAnalysis");
					Display.getDefault().asyncExec(new DisplayStatus(status));
				} finally
				{
					for (Button button : disablingButtons)
					{
						Display.getDefault().asyncExec(new DisplayEnable(button, true));
					}
					Display.getDefault().asyncExec(new DisplayEnable(UiElements.btnAutorun, true));
				}
			}
		}).start();
	}
}
