package com.ciricuervo.detectingpatterns.ui.display;

import org.eclipse.swt.widgets.Control;

/**
 * @author ciricuervo
 *
 */
public class DisplayEnable implements Runnable {

	private Control control;
	private boolean enabled;

	public DisplayEnable(Control control, boolean enabled) {
		this.control = control;
		this.enabled = enabled;
	}

	@Override
	public void run()
	{
		try
		{
			control.setEnabled(enabled);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
