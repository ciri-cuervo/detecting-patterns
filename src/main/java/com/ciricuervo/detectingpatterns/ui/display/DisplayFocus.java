package com.ciricuervo.detectingpatterns.ui.display;

import org.eclipse.swt.widgets.Control;

/**
 * @author ciricuervo
 *
 */
public class DisplayFocus implements Runnable {

	private Control control;

	public DisplayFocus(Control control) {
		this.control = control;
	}

	@Override
	public void run()
	{
		try
		{
			control.setFocus();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
