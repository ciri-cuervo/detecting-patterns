package com.ciricuervo.detectingpatterns.ui.display;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.ciricuervo.detectingpatterns.Pattern;
import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.ui.UiElements;

/**
 * @author ciricuervo
 *
 */
public class DisplayPattern implements Runnable {

	private static final int MAX_DISPLAY = 6;

	private int colIndex;
	private List<Pattern> patterns;
	private Set<Integer> values;
	private long more;

	public DisplayPattern(int colIndex, List<Pattern> patterns, Set<Integer> values, long patternsCount) {
		this.colIndex = colIndex;
		int max = Math.min(patterns.size(), MAX_DISPLAY);
		this.patterns = new ArrayList<Pattern>(patterns.subList(0, max));
		this.values = values;
		this.more = patternsCount - MAX_DISPLAY;
	}

	@Override
	public void run()
	{
		try
		{
			// draw values in table

			StringBuilder tableText = new StringBuilder(16);
			for (Integer integer : values)
			{
				if (tableText.length() != 0)
				{
					tableText.append("/");
				}
				tableText.append(integer.toString());
			}

			UiElements.tblCandidateRow.getItem(0).setText(colIndex + 2, tableText.toString());

			// draw patterns in report list

			StringBuilder labelText = new StringBuilder(256);
			labelText.append(Messages.getString("Other.column", colIndex + 1));
			if (!patterns.isEmpty())
			{
				if (colIndex < 9)
				{
					labelText.append("  ");
				}
				labelText.append("    -").append(patterns.get(0).getDistance()).append("-");
				for (Pattern pattern : patterns)
				{
					pattern.invert();
					labelText.append("    ").append(pattern);
				}
			}

			if (more > 0)
			{
				labelText.append("    ").append(Messages.getString("Other.andMore", more));
			}

			UiElements.lblsColumnsDetail.get(colIndex).setText(labelText.toString());

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
