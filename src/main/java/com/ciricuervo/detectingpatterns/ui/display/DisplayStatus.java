package com.ciricuervo.detectingpatterns.ui.display;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Listener;

import com.ciricuervo.detectingpatterns.ui.UiElements;

/**
 * @author ciricuervo
 *
 */
public class DisplayStatus implements Runnable {

	private String status;
	private String link;

	public DisplayStatus() {
		this("");
	}

	public DisplayStatus(String status) {
		this(status, null);
	}

	public DisplayStatus(String status, String link) {
		this.status = status;
		this.link = link;
	}

	@Override
	public void run()
	{
		try
		{
			UiElements.lnkStatus.setText(status);

			for (Listener listener : UiElements.lnkStatus.getListeners(SWT.Selection))
			{
				UiElements.lnkStatus.removeListener(SWT.Selection, listener);
			}

			if (link != null)
			{
				UiElements.lnkStatus.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e)
					{
						try
						{
							Desktop.getDesktop().open(new File(link));
						} catch (IOException e1)
						{
							e1.printStackTrace();
						}
					}
				});
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
