package com.ciricuervo.detectingpatterns.ui.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * @author ciricuervo
 *
 */
public class SettingsManager {

	private static final Properties PROPS = new Properties();
	private static final String DOWNLOADS_DIR = "downloadsDir";
	private static final String SELECTED_FILE = "selectedFile";
	private static final String PROPERTIES_PATH = "Detecting Patterns.properties";

	static
	{
		readPropertiesFile();
	}

	public static String getDownloadsDir()
	{
		String downloadsDir = PROPS.getProperty(DOWNLOADS_DIR);
		if (downloadsDir != null && !downloadsDir.isEmpty())
		{
			return downloadsDir;
		}

		return getSelectedFileDirectory();
	}

	public static void setDownloadsDir(String downloadsDir)
	{
		PROPS.setProperty(DOWNLOADS_DIR, downloadsDir);
		savePropertiesFile();
	}

	public static String getSelectedFile()
	{
		String selectedFile = PROPS.getProperty(SELECTED_FILE);
		if (selectedFile == null || selectedFile.isEmpty())
		{
			return null;
		}

		return selectedFile;
	}

	public static void setSelectedFile(String selectedFile)
	{
		PROPS.setProperty(SELECTED_FILE, selectedFile);
		savePropertiesFile();
	}

	public static String getSelectedFileDirectory()
	{
		String selectedFile = PROPS.getProperty(SELECTED_FILE);
		if (selectedFile == null || selectedFile.isEmpty())
		{
			return null;
		}

		int slash = selectedFile.lastIndexOf("\\");
		if (slash == -1)
		{
			slash = selectedFile.lastIndexOf("/");
		}

		return selectedFile.substring(0, slash);
	}

	private static String getReportPath(String sufix)
	{
		String selectedFile = PROPS.getProperty(SELECTED_FILE);
		if (selectedFile == null || selectedFile.isEmpty())
		{
			return null;
		}

		int slash = selectedFile.lastIndexOf("\\");
		if (slash == -1)
		{
			slash = selectedFile.lastIndexOf("/");
		}

		String fullName = selectedFile.substring(slash + 1);

		int dot = fullName.lastIndexOf(".");
		String fileName = fullName.substring(0, dot);
		String extension = fullName.substring(dot + 1);

		return getDownloadsDir() + File.separator + fileName + sufix + "." + extension;
	}

	public static String getReportPath()
	{
		return getReportPath(" - report");
	}

	public static String getAutorunReportPath()
	{
		return getReportPath(" - autorun report");
	}

	private static void readPropertiesFile()
	{
		InputStream input = null;
		try
		{
			input = new FileInputStream(PROPERTIES_PATH);
			// load a properties file
			PROPS.load(input);

		} catch (IOException io)
		{
			// mute
		} finally
		{
			if (input != null)
			{
				try
				{
					input.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	private static void savePropertiesFile()
	{
		OutputStream output = null;
		try
		{
			output = new FileOutputStream(PROPERTIES_PATH);
			// save properties to project root folder
			PROPS.store(output, null);

		} catch (IOException io)
		{
			io.printStackTrace();
		} finally
		{
			if (output != null)
			{
				try
				{
					output.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

}
