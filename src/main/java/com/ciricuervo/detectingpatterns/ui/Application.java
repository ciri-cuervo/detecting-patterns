package com.ciricuervo.detectingpatterns.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.ciricuervo.detectingpatterns.msg.Messages;
import com.ciricuervo.detectingpatterns.ui.listeners.AddNewRowListener;
import com.ciricuervo.detectingpatterns.ui.listeners.AutorunListener;
import com.ciricuervo.detectingpatterns.ui.listeners.BackspacePressedListener;
import com.ciricuervo.detectingpatterns.ui.listeners.CopyToNewRowListener;
import com.ciricuervo.detectingpatterns.ui.listeners.ManualInputListener;
import com.ciricuervo.detectingpatterns.ui.listeners.ModifyNewRowListener;
import com.ciricuervo.detectingpatterns.ui.listeners.OpenExcelFileListener;
import com.ciricuervo.detectingpatterns.ui.listeners.RemoveLastRowListener;
import com.ciricuervo.detectingpatterns.ui.listeners.StartPatternsDetectionListener;
import com.ciricuervo.detectingpatterns.ui.listeners.StorageFolderListener;

/**
 * @author ciricuervo
 *
 */
public class Application {

	private static final String VERSION = "v1.1.3";
	private static final int LAST_ROWS = 8;
	private static final int STATUS_LABEL_Y = 30;
	private static final int STATUS_LABEL_SPACING = 26;

	public static final String RESOURCES_DIR = ""; // resources/
	public static final String IMG_DIR = "/" + RESOURCES_DIR + "images";

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		try
		{
			Application window = new Application();
			window.open();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		System.exit(0);
	}

	/**
	 * Open the window.
	 */
	public void open()
	{
		Display display = Display.getDefault();
		createContents();

		UiElements.btnOpenExcelFile.setFocus();

		UiElements.shlDetectingPatterns.open();
		UiElements.shlDetectingPatterns.layout();
		while (!UiElements.shlDetectingPatterns.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents()
	{
		UiElements.shlDetectingPatterns.setSize(1088, 678);
		UiElements.shlDetectingPatterns.setText("Detecting Patterns " + VERSION);
		UiElements.shlDetectingPatterns.setImage(SWTResourceManager.getImage(Application.class,
				IMG_DIR + "/workflow-64.png"));

		/*
		 * Buttons
		 */

		UiElements.btnStorageFolder.setBounds(10, 15, 56, 36);
		UiElements.btnStorageFolder.setImage(SWTResourceManager.getImage(Application.class, IMG_DIR
				+ "/downloads-folder-26.png"));
		UiElements.btnStorageFolder.setToolTipText(Messages
				.getString("Application.btnStorageFolder.text")); //$NON-NLS-1$
		UiElements.btnStorageFolder.addSelectionListener(new StorageFolderListener());

		final Label sep1 = new Label(UiElements.shlDetectingPatterns, SWT.SEPARATOR | SWT.VERTICAL);
		sep1.setBounds(74, 15, 2, 36);

		UiElements.btnOpenExcelFile.setBounds(82, 15, 56, 36);
		UiElements.btnOpenExcelFile.setImage(SWTResourceManager.getImage(Application.class, IMG_DIR
				+ "/ms-excel-26.png"));
		UiElements.btnOpenExcelFile.setToolTipText(Messages
				.getString("Application.btnOpenExcelFile.text")); //$NON-NLS-1$
		UiElements.btnOpenExcelFile.addSelectionListener(new OpenExcelFileListener());

		UiElements.btnManualInput.setBounds(146, 15, 56, 36);
		UiElements.btnManualInput.setImage(SWTResourceManager.getImage(Application.class, IMG_DIR
				+ "/one-finger-25.png"));
		UiElements.btnManualInput.setToolTipText(Messages
				.getString("Application.btnManualInput.text")); //$NON-NLS-1$
		UiElements.btnManualInput.addSelectionListener(new ManualInputListener());

		final Label sep2 = new Label(UiElements.shlDetectingPatterns, SWT.SEPARATOR | SWT.VERTICAL);
		sep2.setBounds(210, 15, 2, 36);

		UiElements.btnStartPatternsDetection.setBounds(218, 15, 56, 36);
		UiElements.btnStartPatternsDetection.setImage(SWTResourceManager.getImage(
				Application.class, IMG_DIR + "/start-26.png"));
		UiElements.btnStartPatternsDetection.setToolTipText(Messages
				.getString("Application.btnStartPatternsDetection.text")); //$NON-NLS-1$
		UiElements.btnStartPatternsDetection
				.addSelectionListener(new StartPatternsDetectionListener(
						UiElements.btnOpenExcelFile, UiElements.btnManualInput,
						UiElements.btnAutorun, UiElements.btnAddNewRow,
						UiElements.btnRemoveLastRow, UiElements.btnCopy));

		UiElements.btnAutorun.setBounds(280, 15, 56, 36);
		UiElements.btnAutorun.setImage(SWTResourceManager.getImage(Application.class, IMG_DIR
				+ "/run-command-26.png"));
		UiElements.btnAutorun.setToolTipText(Messages.getString("Application.btnAutorun.text")); //$NON-NLS-1$
		UiElements.btnAutorun.addSelectionListener(new AutorunListener(UiElements.btnOpenExcelFile,
				UiElements.btnManualInput, UiElements.btnStartPatternsDetection,
				UiElements.btnAddNewRow, UiElements.btnRemoveLastRow, UiElements.btnCopy));

		/*
		 * Add new row
		 */

		UiElements.grpAddNewRow.setBounds(10, 76, 585, 78);
		UiElements.grpAddNewRow.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		UiElements.grpAddNewRow.setText(Messages.getString("Application.grpAddNewRow.text")); //$NON-NLS-1$

		UiElements.btnAddNewRow.setBounds(476, 26, 56, 36);
		UiElements.btnAddNewRow.setImage(SWTResourceManager.getImage(Application.class, IMG_DIR
				+ "/add-list-24.png"));
		UiElements.btnAddNewRow.setToolTipText(Messages.getString("Application.btnAddNewRow.text")); //$NON-NLS-1$
		UiElements.btnAddNewRow.addSelectionListener(new AddNewRowListener());

		UiElements.btnRemoveLastRow.setBounds(540, 26, 36, 36);
		UiElements.btnRemoveLastRow.setImage(SWTResourceManager.getImage(Application.class, IMG_DIR
				+ "/delete-25.png"));
		UiElements.btnRemoveLastRow.setToolTipText(Messages
				.getString("Application.btnRemoveLastRow.text")); //$NON-NLS-1$
		UiElements.btnRemoveLastRow.addSelectionListener(new RemoveLastRowListener());

		/*
		 * Candidate new row
		 */

		UiElements.grpCandidateNewRow.setBounds(10, 178, 585, 78);
		UiElements.grpCandidateNewRow.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		UiElements.grpCandidateNewRow.setText(Messages
				.getString("Application.grpCandidateNewRow.text")); //$NON-NLS-1$

		UiElements.tblCandidateRow.setBounds(10, 30, 500, 28);

		final TableColumn row0 = new TableColumn(UiElements.tblCandidateRow, SWT.CENTER);
		row0.setWidth(0);
		row0.setResizable(false);

		final TableColumn row1 = new TableColumn(UiElements.tblCandidateRow, SWT.CENTER);
		row1.setWidth(60);
		row1.setResizable(false);

		UiElements.btnCopy.setBounds(520, 26, 56, 36);
		UiElements.btnCopy.setImage(SWTResourceManager.getImage(Application.class, IMG_DIR
				+ "/copy-26.png"));
		UiElements.btnCopy.setToolTipText(Messages.getString("Application.btnCopy.text")); //$NON-NLS-1$
		UiElements.btnCopy.addSelectionListener(new CopyToNewRowListener());

		/*
		 * Last rows
		 */

		UiElements.grpLastRows.setBounds(638, 10, 434, 246);
		UiElements.grpLastRows.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		UiElements.grpLastRows.setText(Messages.getString("Application.grpLastRows.text")); //$NON-NLS-1$

		for (int rowIndex = 0; rowIndex < LAST_ROWS; rowIndex++)
		{
			int yPosition = 36 + (rowIndex * 26);

			Label label = new Label(UiElements.grpLastRows, SWT.NONE);
			label.setBounds(10, yPosition, 410, 22);
			label.setFont(SWTResourceManager.getFont("Lucida Console", 9, SWT.NONE));
			UiElements.lblsLastRows.add(label);
		}

		/*
		 * Columns
		 */

		UiElements.grpColumns.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		UiElements.grpColumns.setText(Messages.getString("Application.grpColumns.text")); //$NON-NLS-1$
		UiElements.grpColumns.setBounds(10, 279, 1062, 318);

		/*
		 * Status
		 */

		UiElements.lnkStatus.setBounds(20, 608, 1052, 25);
	}

	public static void cleanCandidateRowItems()
	{
		// remove items
		UiElements.tblCandidateRow.removeAll();

		// add a new items and set rows number
		new TableItem(UiElements.tblCandidateRow, SWT.NONE);
		UiElements.tblCandidateRow.getItem(0).setText(1,
				"-" + (UiElements.array.getRows() + 1) + "-");
		UiElements.tblCandidateRow.getItem(0).setFont(1,
				SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
	}

	public static void refreshLastRows()
	{
		int maxRows = Math.min(UiElements.array.getRows(), LAST_ROWS);
		int offset = LAST_ROWS - maxRows;

		for (int i = 0; i < UiElements.lblsLastRows.size(); i++)
		{
			int rowIndex = UiElements.array.getRows() - 1 - i + offset;
			int labelIndex = UiElements.lblsLastRows.size() - 1 - i;
			Label label = UiElements.lblsLastRows.get(labelIndex);

			if (labelIndex < maxRows)
			{
				StringBuilder text = new StringBuilder(64);
				if (rowIndex < 9)
				{
					text.append(" ");
				}
				text.append("    -").append(rowIndex + 1).append("-  ");
				for (int colIndex = 0; colIndex < UiElements.array.getCols(); colIndex++)
				{
					text.append("  ").append(UiElements.array.getValue(rowIndex, colIndex));
				}
				label.setText(text.toString());
			} else
			{
				label.setText("");
			}
		}
	}

	public static void createComponents()
	{
		// remove new row texts
		for (Text text : UiElements.txtsNewRow)
		{
			text.dispose();
		}

		// clear list
		UiElements.txtsNewRow.clear();

		// add new row texts
		Control[] oldRowConrol = UiElements.grpAddNewRow.getTabList();
		Control[] newRowConrol = new Control[UiElements.array.getCols() + 2];
		for (int i = 1; i <= 2; i++)
		{
			newRowConrol[newRowConrol.length - i] = oldRowConrol[oldRowConrol.length - i];
		}
		for (int i = 0; i < UiElements.array.getCols(); i++)
		{
			final Text text = new Text(UiElements.grpAddNewRow, SWT.BORDER | SWT.CENTER);
			text.setBounds(10 + 42 * i, 33, 36, 26);
			text.setTextLimit(1);
			UiElements.txtsNewRow.add(text);
			newRowConrol[i] = text;

			if (i > 0)
			{
				UiElements.txtsNewRow.get(i - 1).addModifyListener(new ModifyNewRowListener(text));
				text.addKeyListener(new BackspacePressedListener(UiElements.txtsNewRow.get(i - 1)));
			}
		}

		if (!UiElements.txtsNewRow.isEmpty())
		{
			Text lastText = UiElements.txtsNewRow.get(UiElements.txtsNewRow.size() - 1);
			lastText.addModifyListener(new ModifyListener() {
				public void modifyText(ModifyEvent e)
				{
					Text text = (Text) e.widget;
					if (text.getText().length() > 0)
					{
						UiElements.btnAddNewRow.setFocus();
					}
				}
			});

			Listener[] listeners = UiElements.btnAddNewRow.getListeners(SWT.KeyDown);
			for (Listener listener : listeners)
			{
				UiElements.btnAddNewRow.removeListener(SWT.KeyDown, listener);
			}
			UiElements.btnAddNewRow.addKeyListener(new BackspacePressedListener(lastText));

			UiElements.txtsNewRow.get(0).setFocus();
		}

		UiElements.grpAddNewRow.setTabList(newRowConrol);

		// remove columns
		while (UiElements.tblCandidateRow.getColumnCount() > 2)
		{
			UiElements.tblCandidateRow.getColumns()[2].dispose();
		}

		// add new columns
		if (UiElements.array.getCols() > 0)
		{
			int width = 436 / UiElements.array.getCols();
			for (int i = 0; i < UiElements.array.getCols(); i++)
			{
				TableColumn column = new TableColumn(UiElements.tblCandidateRow, SWT.CENTER);
				column.setWidth(width);
				column.setText(String.valueOf(i + 1));
			}
		}

		// clean items
		Application.cleanCandidateRowItems();

		// remove all label status
		for (Label label : UiElements.lblsColumnsDetail.values())
		{
			label.dispose();
		}

		// clear hash map
		UiElements.lblsColumnsDetail.clear();

		// create status labels
		for (int i = 0; i < UiElements.array.getCols(); i++)
		{
			int yPosition = STATUS_LABEL_Y + (i * STATUS_LABEL_SPACING);

			Label label = new Label(UiElements.grpColumns, SWT.NONE);
			label.setBounds(10, yPosition, 1050, 20);
			label.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.NONE));
			UiElements.lblsColumnsDetail.put(i, label);
		}

		Application.refreshLastRows();
		Application.cleanColumnsDetailLabels();
	}

	public static void cleanColumnsDetailLabels()
	{
		for (int i = 0; i < UiElements.lblsColumnsDetail.size(); i++)
		{
			Label label = UiElements.lblsColumnsDetail.get(i);
			label.setText(Messages.getString("Other.column", i + 1));
		}
	}

}
