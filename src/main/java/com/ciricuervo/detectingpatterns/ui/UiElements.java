package com.ciricuervo.detectingpatterns.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

import com.ciricuervo.detectingpatterns.Array;

/**
 * @author ciricuervo
 *
 */
public class UiElements {

	public static final Shell shlDetectingPatterns = new Shell(SWT.SHELL_TRIM
			& ~(SWT.RESIZE | SWT.MAX));
	public static final Button btnStorageFolder = new Button(shlDetectingPatterns, SWT.NONE);
	public static final Button btnOpenExcelFile = new Button(shlDetectingPatterns, SWT.NONE);
	public static final Button btnManualInput = new Button(shlDetectingPatterns, SWT.NONE);
	public static final Button btnStartPatternsDetection = new Button(shlDetectingPatterns,
			SWT.NONE);
	public static final Button btnAutorun = new Button(shlDetectingPatterns, SWT.NONE);
	public static final Group grpAddNewRow = new Group(shlDetectingPatterns, SWT.NONE);
	public static final Button btnAddNewRow = new Button(UiElements.grpAddNewRow, SWT.NONE);
	public static final Button btnRemoveLastRow = new Button(UiElements.grpAddNewRow, SWT.NONE);
	public static final Group grpCandidateNewRow = new Group(shlDetectingPatterns, SWT.NONE);
	public static final List<Text> txtsNewRow = new ArrayList<Text>(12);
	public static final Table tblCandidateRow = new Table(grpCandidateNewRow, SWT.BORDER);
	public static final Button btnCopy = new Button(grpCandidateNewRow, SWT.NONE);
	public static final Group grpLastRows = new Group(shlDetectingPatterns, SWT.NONE);
	public static final List<Label> lblsLastRows = new ArrayList<Label>(10);
	public static final Group grpColumns = new Group(shlDetectingPatterns, SWT.NONE);
	public static final HashMap<Integer, Label> lblsColumnsDetail = new HashMap<Integer, Label>(16);
	public static final Link lnkStatus = new Link(shlDetectingPatterns, SWT.NONE);

	public static Array array;

	static
	{
		grpAddNewRow.setTabList(new Control[] { btnAddNewRow, btnRemoveLastRow });
		grpCandidateNewRow.setTabList(new Control[] { btnCopy });
		shlDetectingPatterns.setTabList(new Control[] { btnStorageFolder, btnOpenExcelFile,
				btnManualInput, btnStartPatternsDetection, btnAutorun, grpAddNewRow,
				grpCandidateNewRow, grpLastRows, grpColumns, lnkStatus });
	}

}
