package com.ciricuervo.detectingpatterns.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.ciricuervo.detectingpatterns.msg.Messages;

/**
 * @author ciricuervo
 *
 */
public class ManualInputDialog extends Dialog {

	protected int result;
	protected Shell shlNumberOfColumns;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 */
	public ManualInputDialog(Shell shldetectingpatterns) {
		this(shldetectingpatterns, SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL);
	}

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public ManualInputDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public int open()
	{
		createContents();
		shlNumberOfColumns.open();
		shlNumberOfColumns.layout();
		Display display = getParent().getDisplay();
		while (!shlNumberOfColumns.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents()
	{
		shlNumberOfColumns = new Shell(getParent(), getStyle());
		shlNumberOfColumns.setSize(441, 175);
		shlNumberOfColumns.setText(Messages.getString("ManualInput.shlNumberOfColumns.text")); //$NON-NLS-1$

		final Label lblNuumberOfColumns = new Label(shlNumberOfColumns, SWT.NONE);
		lblNuumberOfColumns.setBounds(86, 31, 148, 20);
		lblNuumberOfColumns.setText(Messages.getString("ManualInput.lblNuumberOfColumns.text")); //$NON-NLS-1$

		final Text text = new Text(shlNumberOfColumns, SWT.BORDER);
		text.setBounds(258, 28, 68, 26);

		final Button btnOk = new Button(shlNumberOfColumns, SWT.NONE);
		btnOk.setBounds(116, 88, 90, 30);
		btnOk.setText(Messages.getString("ManualInput.btnOk.text")); //$NON-NLS-1$
		btnOk.setEnabled(false);

		final Button btnCancel = new Button(shlNumberOfColumns, SWT.NONE);
		btnCancel.setBounds(212, 88, 90, 30);
		btnCancel.setText(Messages.getString("ManualInput.btnCancel.text")); //$NON-NLS-1$

		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e)
			{
				try
				{
					result = new Integer(text.getText());
					btnOk.setEnabled(true);
				} catch (Exception e1)
				{
					btnOk.setEnabled(false);
				}
			}
		});

		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				shlNumberOfColumns.dispose();
			}
		});

		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				result = -1;
				shlNumberOfColumns.dispose();
			}
		});

		shlNumberOfColumns.addTraverseListener(new TraverseListener() {
			@Override
			public void keyTraversed(TraverseEvent e)
			{
				if (e.detail == SWT.TRAVERSE_RETURN)
				{
					if (btnOk.isEnabled())
					{
						shlNumberOfColumns.dispose();
					}
				}
			}
		});

	}
}
