package com.ciricuervo.detectingpatterns;

/**
 * @author ciricuervo
 *
 */
public class AutorunCollection {

	public enum Match {
		FALSE, SINGLE, MULTI
	}

	private Pattern[][] patterns;
	private Match[][] patternMatch;
	private int rows;
	private int cols;

	public AutorunCollection(int rows, int cols) {
		this.patterns = new Pattern[rows][cols];
		this.patternMatch = new Match[rows][cols];
		this.rows = rows;
		this.cols = cols;
	}

	public void set(int rowIndex, int colIndex, Pattern pattern, Match match)
	{
		patterns[rowIndex][colIndex] = pattern;
		patternMatch[rowIndex][colIndex] = match;
	}

	public void setMatch(int rowIndex, int colIndex, Match match)
	{
		patternMatch[rowIndex][colIndex] = match;
	}

	public Pattern getPattern(int rowIndex, int colIndex)
	{
		return patterns[rowIndex][colIndex];
	}

	public Match getPatternMatch(int rowIndex, int colIndex)
	{
		return patternMatch[rowIndex][colIndex];
	}

	public int getRows()
	{
		return rows;
	}

	public void setRows(int rows)
	{
		this.rows = rows;
	}

	public int getCols()
	{
		return cols;
	}

	public void setCols(int cols)
	{
		this.cols = cols;
	}

	public Match getRowMatch(int rowIndex)
	{
		boolean hasMulti = false;
		for (Match match : patternMatch[rowIndex])
		{
			if (match == null || Match.FALSE.equals(match))
			{
				return Match.FALSE;
			}
			if (Match.MULTI.equals(match))
			{
				hasMulti = true;
			}
		}
		return hasMulti ? Match.MULTI : Match.SINGLE;
	}

	public int getMatchingRows()
	{
		int count = 0;
		for (int rowIndex = 0; rowIndex < rows; rowIndex++)
		{
			if (!Match.FALSE.equals(getRowMatch(rowIndex)))
			{
				count++;
			}
		}
		return count;
	}

}
