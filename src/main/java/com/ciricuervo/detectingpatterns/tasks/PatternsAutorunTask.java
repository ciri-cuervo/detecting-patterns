package com.ciricuervo.detectingpatterns.tasks;

import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.Callable;

import com.ciricuervo.detectingpatterns.AutorunCollection;
import com.ciricuervo.detectingpatterns.AutorunCollection.Match;
import com.ciricuervo.detectingpatterns.Pattern;
import com.ciricuervo.detectingpatterns.PatternsCollection;

/**
 * @author ciricuervo
 *
 */
public class PatternsAutorunTask implements Callable<Match> {

	private int rowIndex;
	private int colIndex;
	private int value;
	private AutorunCollection collection;
	private PatternsCollection patterns;

	public PatternsAutorunTask(int rowIndex, int colIndex, int value, AutorunCollection collection,
			PatternsCollection patterns) {
		this.rowIndex = rowIndex;
		this.colIndex = colIndex;
		this.value = value;
		this.collection = collection;
		this.patterns = patterns;
	}

	@Override
	public Match call() throws Exception
	{
		LinkedList<Pattern> result = patterns.get(colIndex);
		Set<Integer> values = patterns.getValues(colIndex);

		if (result.isEmpty() || !values.contains(value))
		{
			collection.set(rowIndex, colIndex, result.peekFirst(), Match.FALSE);
			return Match.FALSE;
		}

		Match match = values.size() == 1 ? Match.SINGLE : Match.MULTI;

		// if match is SIMPLE, breaks with the first element
		for (Pattern pattern : result)
		{
			if (pattern.getValue() == value)
			{
				collection.set(rowIndex, colIndex, pattern, match);
				return match;
			}
		}

		// increase MAX_PATTERNS_QUEUE in order to find the pattern for 'value'
		collection.set(rowIndex, colIndex, result.peekFirst(), match);
		return match;
	}

}
