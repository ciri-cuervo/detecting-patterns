package com.ciricuervo.detectingpatterns.tasks;

import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import com.ciricuervo.detectingpatterns.Pattern;
import com.ciricuervo.detectingpatterns.PatternsCollection;
import com.ciricuervo.detectingpatterns.ui.display.DisplayPattern;

/**
 * @author ciricuervo
 *
 */
public class PatternsDisplayTask implements Callable<DisplayPattern> {

	private int colIndex;
	private PatternsCollection patterns;

	public PatternsDisplayTask(int colIndex, PatternsCollection patterns) {
		this.colIndex = colIndex;
		this.patterns = patterns;
	}

	@Override
	public DisplayPattern call() throws Exception
	{
		List<Pattern> result = patterns.get(colIndex);
		Set<Integer> values = patterns.getValues(colIndex);
		long patternsCount = patterns.getPatternsCount(colIndex);
		return new DisplayPattern(colIndex, result, values, patternsCount);
	}

}
