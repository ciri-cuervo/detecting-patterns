package com.ciricuervo.detectingpatterns.tasks;

import java.util.concurrent.Callable;

import com.ciricuervo.detectingpatterns.Array;
import com.ciricuervo.detectingpatterns.Pattern;
import com.ciricuervo.detectingpatterns.PatternsCollection;
import com.ciricuervo.detectingpatterns.PatternsDetector;

/**
 * @author ciricuervo
 *
 */
public class PatternsDetectorTask implements Callable<Pattern> {

	private Array array;
	private int length;
	private int value;
	private int solvingRow;
	private int colIndex;
	private PatternsCollection patterns;

	public PatternsDetectorTask(Array array, int length, int value, int colIndex, int rowIndex,
			PatternsCollection patterns) {
		this.array = array;
		this.length = length;
		this.value = value;
		this.solvingRow = rowIndex;
		this.colIndex = colIndex;
		this.patterns = patterns;
	}

	@Override
	public Pattern call() throws Exception
	{
		try
		{
			if (solvingRow + 1 >= length && PatternsDetector.isInArrayDomain(value))
			{
				Pattern pattern = new Pattern(value, length, colIndex);
				detectPatterns(pattern, solvingRow - 1);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	private void detectPatterns(Pattern pattern, int rowIndex)
	{
		if (pattern.getColIndices().size() == pattern.getLength())
		{
			int colIndex = pattern.getColIndices().get(0);
			int initialRow = getPatternStart(pattern, rowIndex);
			int distance = solvingRow - initialRow + 1;
			pattern.setDistance(distance);
			patterns.add(colIndex, pattern);
			return;
		}

		for (int colIndex = 0; colIndex < array.getCols(); colIndex++)
		{
			if (array.getValue(rowIndex, colIndex) != pattern.getValue())
			{
				continue;
			}

			Pattern nextResult = (Pattern) pattern.clone();
			nextResult.getColIndices().add(colIndex);

			detectPatterns(nextResult, rowIndex - 1);
		}
	}

	private int getPatternStart(Pattern pattern, int rowIndex)
	{
		int colIndicesIndex = 0;
		for (; rowIndex > -1; rowIndex--)
		{
			int colIndex = pattern.getColIndices().get(colIndicesIndex);
			if (array.getValue(rowIndex, colIndex) != pattern.getValue())
			{
				break;
			}
			colIndicesIndex = (colIndicesIndex + 1) % pattern.getLength();
		}

		return rowIndex + 1;
	}

}
