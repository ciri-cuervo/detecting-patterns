package com.ciricuervo.detectingpatterns;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ciricuervo
 *
 */
public class Pattern implements Cloneable {

	private int value;
	private int length;
	private List<Integer> colIndices;

	private int distance;
	private double repetitions;
	private boolean inverted;

	public Pattern(int value, int patternLength, int colIndex) {
		this.value = value;
		this.length = patternLength;
		this.colIndices = new ArrayList<Integer>(16);
		this.colIndices.add(colIndex);
		this.inverted = false;
	}

	public int getValue()
	{
		return value;
	}

	public void setValue(int value)
	{
		this.value = value;
	}

	public int getLength()
	{
		return length;
	}

	public void setLength(int length)
	{
		this.length = length;
	}

	public List<Integer> getColIndices()
	{
		return colIndices;
	}

	public void setColIndices(List<Integer> colIndices)
	{
		this.colIndices = colIndices;
	}

	public int getDistance()
	{
		return distance;
	}

	public void setDistance(int distance)
	{
		this.distance = distance;
		if (length != 0)
		{
			this.repetitions = (double) distance / length;
		}
	}

	public double getRepetitions()
	{
		return repetitions;
	}

	public boolean isInverted()
	{
		return inverted;
	}

	public void invert()
	{
		if (!inverted)
		{
			int shift = distance % length;
			shiftLeftAndInvert(colIndices, shift);
			inverted = true;
		}
	}

	@Override
	public Object clone()
	{
		Pattern cloned = null;
		try
		{
			cloned = (Pattern) super.clone();
			cloned.setColIndices(new ArrayList<Integer>(cloned.getColIndices()));
		} catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
		}
		return cloned;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colIndices == null) ? 0 : colIndices.hashCode());
		result = prime * result + length;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pattern other = (Pattern) obj;
		if (colIndices == null)
		{
			if (other.colIndices != null)
				return false;
		} else if (!colIndices.equals(other.colIndices))
			return false;
		if (length != other.length)
			return false;
		if (value != other.value)
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		StringBuilder indices = new StringBuilder(32);
		for (Integer colIndex : colIndices)
		{
			indices.append(colIndex + 1);
		}
		return value + "'-(" + indices + ") : " + DF.format(repetitions);
	}

	private static final DecimalFormat DF = new DecimalFormat("#,##0.##");

	public static <E> void shiftLeftAndInvert(List<E> list, int shift)
	{
		Collections.rotate(list, -shift);
		Collections.reverse(list);
	}

}
